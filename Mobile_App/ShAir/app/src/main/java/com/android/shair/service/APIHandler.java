package com.android.shair.service;

import android.content.Context;
import android.os.Handler;

import com.android.shair.handlers.DeviceDisconnectHandler;
import com.android.shair.data.PreferencesManager;
import com.android.shair.models.BTDevice;
import com.android.shair.service.responses.BaseResponse;
import com.android.shair.service.responses.InfoResponse;
import com.android.shair.ui.fragments.Devices.DevicesFragment;
import com.android.shair.ui.fragments.TemperatureFragment;
import com.android.shair.models.Device;
import com.android.shair.service.utils.HttpRequestHandler;
import com.android.shair.service.utils.RequestRunnable;
import com.android.shair.utils.Utils;
//import com.android.shair.service.utils.JSONHttpClient;

//import org.apache.http.NameValuePair;
//import org.apache.http.message.BasicNameValuePair;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Callable;

public class APIHandler {
    private static final String BASE_SHAIR_API_URL = "https://shairclimatecontrol.azurewebsites.net/";
    private static final String SHAIR_API_CONNECT = "user/connect";
    private static final String SHAIR_API_DISCONNECT = "user/disconnect";
    private static final String SHAIR_API_REFRESH = "user/refresh";
    private static final String SHAIR_API_LOCATION = "user/location?btMac=";
    private static final String SHAIR_API_CHANGE_TEMPERATURE = "user/changetmp";
    private static APIHandler single_instance = null;

    private APIHandler() {
    }

    public static APIHandler getInstance() {
        if (single_instance == null) {
            single_instance = new APIHandler();
        }
        return single_instance;
    }

    private static String getAPIUrl(String partialUrl) {
        return BASE_SHAIR_API_URL.concat(partialUrl);
    }

    // Connect the device using bluetooth id
    public void connect(final Context context, final BTDevice btDevice) {
        Thread thread = new Thread(new RequestRunnable(DevicesFragment.deviceConnectHandler, new Callable<Object>() {
            public Object call() throws Exception {
                String androidId = Utils.getAndroidId(context);
                JSONObject jsonData = new JSONObject();
                jsonData.put("userMac", androidId);
                JSONObject jsonBT = new JSONObject();
                jsonBT.put("btMac", btDevice.getBtMac());
                jsonData.put("location", jsonBT);
                String url = getAPIUrl(SHAIR_API_CONNECT);

                BaseResponse response = HttpRequestHandler.postJSON(url, jsonData);
                return new InfoResponse(response, btDevice);
            }
        }));
        thread.start();
    }

    // Get location for the bluetooth device
    public void location(final Context context, Handler handler, final String btMac, final String btName) {
        Thread thread = new Thread(new RequestRunnable(handler, new Callable<Object>() {
            public Object call() throws JSONException {
                String url = getAPIUrl(SHAIR_API_LOCATION).concat(btMac);
                JSONObject json = HttpRequestHandler.getJSON(url);
                String location  = ResponseParser.getInstance().parseJsonLocation(json);
                return new BTDevice(btMac, btName, location);
            }
        }));
        thread.start();
    }

    // Get updated device info
    public void refresh(final Context context, Handler handler, final BTDevice baseDeviceInfo) {
        Thread thread = new Thread(new RequestRunnable(handler, new Callable<Object>() {
            public Object call() throws JSONException {
                Device deviceInfo =  PreferencesManager.getDevice(context);

                String androidId = Utils.getAndroidId(context);
                JSONObject jsonData = new JSONObject();
                jsonData.put("userMac", androidId);
                String url = getAPIUrl(SHAIR_API_REFRESH);

                BaseResponse response = HttpRequestHandler.postJSON(url, jsonData);
                InfoResponse infoResponse = new InfoResponse(response, baseDeviceInfo);
                if(infoResponse.isSuccess() && deviceInfo != null){
                    infoResponse.getDeviceInfo().setRequestedTemp(deviceInfo.getRequestedTemp());
                }
                return infoResponse;
            }
        }));
        thread.start();
    }

    // Disconnect the device
    public void disconnect(final Context context, DeviceDisconnectHandler handler,final BTDevice connectNext) {
        Thread thread = new Thread(new RequestRunnable(handler, new Callable<Object>() {
            public Object call() throws JSONException {
                String androidId = Utils.getAndroidId(context);
                JSONObject jsonData = new JSONObject();
                jsonData.put("userMac", androidId);
                String url = getAPIUrl(SHAIR_API_DISCONNECT);
                HttpRequestHandler.postJSON(url, jsonData);
                return connectNext;
            }
        }));
        thread.start();
    }

    // Send request for changing the temperature according user vote
    public void changeTemp(final Context context, final BTDevice info, final double userTempRequest) {
        Thread thread = new Thread(new RequestRunnable(TemperatureFragment.sendTemperatureHandler, new Callable<Object>() {
            public Object call() throws JSONException {
                String androidId = Utils.getAndroidId(context);
                Device deviceInfo = null;
                JSONObject jsonData = new JSONObject();
                jsonData.put("userMac", androidId);
                JSONObject jsonBT = new JSONObject();
                jsonBT.put("btMac", info.getBtMac());
                jsonData.put("location", jsonBT);
                jsonData.put("userTmpRequest", userTempRequest);
                String url = getAPIUrl(SHAIR_API_CHANGE_TEMPERATURE);

                BaseResponse response = HttpRequestHandler.postJSON(url, jsonData);
                return new InfoResponse(response, info);
            }
        }));
        thread.start();
    }
}
