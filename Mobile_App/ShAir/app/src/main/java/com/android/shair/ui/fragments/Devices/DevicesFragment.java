package com.android.shair.ui.fragments.Devices;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.shair.R;
import com.android.shair.data.PreferencesManager;
import com.android.shair.handlers.DeviceDisconnectHandler;
import com.android.shair.models.BTDevice;
import com.android.shair.models.Device;
import com.android.shair.service.APIHandler;
import com.android.shair.service.responses.InfoResponse;
import com.android.shair.ui.dialogs.LoaderDialog;
import com.inuker.bluetooth.library.BluetoothClient;
import com.inuker.bluetooth.library.search.SearchRequest;
import com.inuker.bluetooth.library.search.SearchResult;
import com.inuker.bluetooth.library.search.response.SearchResponse;
import com.inuker.bluetooth.library.utils.BluetoothLog;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DevicesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DevicesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DevicesFragment extends Fragment {

    public final static String VALID_DEVICE_NAME_REGEX_PATTERN = "shair.*";

    public static Handler deviceConnectHandler;
    public Handler locationHandler;
    public DeviceDisconnectHandler deviceDisconnectHandler;
    private HashMap<String, BTDevice> mDevices = new HashMap<>();
    private RecyclerView mRecyclerView;
    private View mEmptyLayout;
    private View mScanningLayout;
    private View mContentReadyLayout;
    private OnFragmentInteractionListener mListener;
    private FloatingActionButton mFabScanDevices;
    public LoaderDialog loaderDialog;
    private static Map<String, String> locationsCache = new HashMap<String, String>();
    private BluetoothClient mBluetoothClient;

    enum ContentType {READY, SCANNING, EMPTY}

    ;

    public DevicesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DevicesFragment.
     */
    public static DevicesFragment newInstance() {
        DevicesFragment fragment = new DevicesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDevices = new HashMap<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getPermissions();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_devices, container, false);
    }

    // Ensure location permission for bluetooth search
    private void getPermissions() {
        int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
        if (ActivityCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(),
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
        } else {
            Log.e("DB", "PERMISSION GRANTED");
        }
    }

    // Init all controls
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mRecyclerView = (RecyclerView) getView().findViewById(R.id.rv_devices);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mEmptyLayout = getView().findViewById(R.id.fragment_devices_empty_layout);
        mScanningLayout = getView().findViewById(R.id.fragment_devices_scanning_layout);
        mContentReadyLayout = getView().findViewById(R.id.fragment_devices_content_ready);

        mFabScanDevices = getView().findViewById(R.id.fab_scan_devices);
        setContentByType(ContentType.SCANNING);
        DevicesRecyclerViewAdapter mAdapter = new DevicesRecyclerViewAdapter(getContext(), mDevices, this);
        mRecyclerView.setAdapter(mAdapter);
        mFabScanDevices.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                searchDevice();
            }
        });

        // Start search for bluetooth devices
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                searchDevice();
            }
        }, 400);

        registerHandlers();
        loaderDialog = new LoaderDialog(getContext());
    }

    // Register handlers for handle responses from other threads
    private void registerHandlers() {
        deviceConnectHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                loaderDialog.hide();
                InfoResponse response = (InfoResponse) msg.obj;
                if (msg.what == 1 && response.isSuccess()) { // Success
                    Device info = response.getDeviceInfo();
                    PreferencesManager.updateBTDevice(getContext(), info.getBTDevice());
                    PreferencesManager.updateDevice(getContext(), info);
                }
                updateRecyclerView();
            }
        };

        deviceDisconnectHandler = new DeviceDisconnectHandler(Looper.getMainLooper(), getContext()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.obj != null) {
                    BTDevice connectNext = (BTDevice) msg.obj;
                    APIHandler.getInstance().connect(getContext(), connectNext);
                } else { // finish disconnecting
                    loaderDialog.hide();
                    updateRecyclerView();
                }
            }
        };

        locationHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == 1) { // success
                    BTDevice info = (BTDevice) msg.obj; // bt mac of next connection
                    mDevices.put(info.getBtMac(), info);
                    locationsCache.put(info.getBtMac(), info.getLocation());
                    updateRecyclerView();
                }
            }
        };
    }

    // Get for location asynchronously, update view when ready
    private void loadLocationsAsync() {
        for (Map.Entry<String, BTDevice> entry : mDevices.entrySet()) {
            BTDevice btDevice = entry.getValue();
            String mac = btDevice.getBtMac();
            if (!locationsCache.containsKey(btDevice.getBtMac())) {
                String name = btDevice.getBtName();
                APIHandler.getInstance().location(getContext(), locationHandler, mac, name);
            } else {
                String cachedLocation = locationsCache.get(mac);
                btDevice.setLocation(cachedLocation);
            }
        }
    }

    // Search for available bluetooth devices
    private void searchDevice() {
        // check if bluetooth is enabled
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            setContentByType(DevicesFragment.ContentType.EMPTY);
            String message = getResources().getString(R.string.bluetooth_not_enabled_msg);
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        } else {

            setContentByType(DevicesFragment.ContentType.SCANNING);
            SearchRequest request = new SearchRequest.Builder()
                    .searchBluetoothLeDevice(2000, 1).build();

            mBluetoothClient.search(request, mSearchResponse);
        }
    }

    private final SearchResponse mSearchResponse = new SearchResponse() {
        @Override
        public void onSearchStarted() {
            BluetoothLog.w("MainActivity.onSearchStarted");
            mDevices.clear();
        }

        @Override
        public void onDeviceFounded(SearchResult deviceResult) {
            String btMac = deviceResult.getAddress();
            String btName = deviceResult.getName();
            if (!mDevices.containsKey(btMac) && isValidResult(deviceResult)) {
                mDevices.put(btMac, new BTDevice(btMac, btName, null));
            }
        }

        @Override
        public void onSearchStopped() {
            BluetoothLog.w("MainActivity.onSearchStopped");
            loadLocationsAsync();
            updateRecyclerView();
        }

        @Override
        public void onSearchCanceled() {
            BluetoothLog.w("MainActivity.onSearchCanceled");
            loadLocationsAsync();
            updateRecyclerView();
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        mBluetoothClient.stopSearch();
    }

    // Check if the founded device is compatible for this app
    private boolean isValidResult(SearchResult result) {
        String deviceName = result.getName();
        return Pattern.matches(VALID_DEVICE_NAME_REGEX_PATTERN, deviceName.toLowerCase());
    }

    // Update recycler view with currently available devices
    private void updateRecyclerView() {
        DevicesRecyclerViewAdapter adapter = new DevicesRecyclerViewAdapter(getContext(), mDevices, this);
        mRecyclerView.swapAdapter(adapter, false);
        if (mDevices.size() > 0) {
            setContentByType(ContentType.READY);
        } else {
            setContentByType(ContentType.EMPTY);
        }
    }

    // Change displaying layout
    public void setContentByType(ContentType contentType) {
        switch (contentType) {
            case READY:
                mEmptyLayout.setVisibility(View.GONE);
                mContentReadyLayout.setVisibility(View.VISIBLE);
                mScanningLayout.setVisibility(View.GONE);
                mFabScanDevices.setVisibility(View.VISIBLE);
                break;
            case SCANNING:
                mEmptyLayout.setVisibility(View.GONE);
                mContentReadyLayout.setVisibility(View.GONE);
                mScanningLayout.setVisibility(View.VISIBLE);
                mFabScanDevices.setVisibility(View.INVISIBLE);
                break;
            case EMPTY:
                mEmptyLayout.setVisibility(View.VISIBLE);
                mContentReadyLayout.setVisibility(View.GONE);
                mScanningLayout.setVisibility(View.GONE);
                mFabScanDevices.setVisibility(View.VISIBLE);
                break;
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (mBluetoothClient == null) { // Allow only one instance for bluetooth client
            mBluetoothClient = new BluetoothClient(context);
        }

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onDevicesFragmentInteraction(Uri uri);
    }
}
