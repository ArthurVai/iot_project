package com.android.shair.models;

// Bluetooth device info
public class BTDevice {
    private String btMac;
    private String btName;
    private String location;

    public BTDevice(String btMac, String btName, String location) {
        this.btMac = btMac;
        this.btName = btName;
        this.location = location;
    }

    public String getBtMac() {
        return btMac;
    }

    public void setBtMac(String btMac) {
        this.btMac = btMac;
    }

    public String getBtName() {
        return btName;
    }

    public void setBtName(String btName) {
        this.btName = btName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
