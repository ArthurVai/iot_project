package com.android.shair.service.utils;

import android.util.Log;
import com.android.shair.service.responses.BaseResponse;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpRequestHandler {
    public static BaseResponse postJSON(final String urlAddress, final JSONObject json) {
        boolean success = false;
        String content = null;
        try {
            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);

            Log.i("JSON", json.toString());
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            os.writeBytes(json.toString());

            os.flush();
            os.close();

            int status = conn.getResponseCode();
            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    success = true;
                    content = sb.toString();
                    break;
                default:
                    content = Integer.toString(status);
                    throw new Exception(String.format("Request failed, status code: %d, message: %s",
                            status, conn.getResponseMessage()));
            }

            conn.disconnect();
        } catch (Exception e) {
            content = e.getMessage();
            Log.e("HttpRequestHandler", String.format("postJSON failed: %s", e.getMessage()));
        }

        return new BaseResponse(success, content);
    }

    public static JSONObject getJSON(final String urlAdress) {
        JSONObject result = null;
        try {
            URL url = new URL(urlAdress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.connect();

            int status = conn.getResponseCode();
            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    result = new JSONObject(sb.toString());
                    break;
                default:
                    throw new Exception(String.format("Request failed, status code: %d, message: %s",
                            status, conn.getResponseMessage()));
            }

            conn.disconnect();
        } catch (Exception e) {
            Log.e("HttpRequestHandler", String.format("getJSON failed: %s", e.getMessage()));
        }

        return result;
    }
}
