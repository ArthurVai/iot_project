package com.android.shair.service.responses;

import android.util.Log;

import com.android.shair.models.BTDevice;
import com.android.shair.models.Device;
import com.android.shair.service.ResponseParser;

import org.json.JSONException;
import org.json.JSONObject;

public class InfoResponse extends BaseResponse{
    public final static int NOT_CONNECTED_CODE = 403;
    private Device mDeviceInfo = null;

    public InfoResponse(BaseResponse baseResponse, BTDevice baseDeviceInfo) {
        super(baseResponse.isSuccess(), baseResponse.getContent());

        if(this.isSuccess()){
            this.mDeviceInfo = new Device(baseDeviceInfo);
            JSONObject deviceInfoJson = null;
            try {
                deviceInfoJson = new JSONObject(baseResponse.getContent());
                ResponseParser.getInstance().parseJsonDevice(deviceInfoJson, mDeviceInfo);
            } catch (JSONException e) {
                Log.e("InfoResponse", String.format("Parse failed: %s", e.getMessage()));
            }
        }
    }

    public boolean isDeviceConnected(){
        Log.e("InfoResponse", getContent());
        return !this.isSuccess() && getContent().equals(NOT_CONNECTED_CODE);
    }

    public Device getDeviceInfo(){
        return mDeviceInfo;
    }
}
