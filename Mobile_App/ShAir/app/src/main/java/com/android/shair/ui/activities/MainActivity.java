package com.android.shair.ui.activities;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import it.sephiroth.android.library.bottomnavigation.BottomNavigation;
import com.android.shair.R;
import com.android.shair.ui.fragments.AboutFragment;
import com.android.shair.ui.fragments.Devices.DevicesFragment;
import com.android.shair.ui.fragments.TemperatureFragment;
import com.android.shair.models.Device;

import static com.android.shair.ui.fragments.TemperatureFragment.*;

public class MainActivity extends AppCompatActivity implements OnFragmentInteractionListener, DevicesFragment.OnFragmentInteractionListener, AboutFragment.OnFragmentInteractionListener {

    public static Device currentConnectedDevice = null;
    private BottomNavigation mBottomNavigation;
    public static String deviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createBottomNavigation();
    }

    private void createBottomNavigation(){

        mBottomNavigation = (BottomNavigation) findViewById(R.id.navigation);
        mBottomNavigation.inflateMenu(R.menu.navigation);

        mBottomNavigation.setOnMenuItemClickListener(new BottomNavigation.OnMenuItemSelectionListener() {
            @Override
            public void onMenuItemSelect(int i, int i1, boolean b) {
                switch (i) {
                    case R.id.navigation_temperature:
                        switchFragment(new TemperatureFragment(), "fragment_temperature");
                        break;
                    case R.id.navigation_devices:
                        switchFragment(new DevicesFragment(), "fragment_devices");
                        break;
                    case R.id.navigation_about:
                        switchFragment(new AboutFragment(), "fragment_about");
                        break;
                }
            }

            @Override
            public void onMenuItemReselect(int i, int i1, boolean b) {

            }
        });

        Fragment fragment = new TemperatureFragment();
        setInitialFragment(fragment, 0);
        switchFragment(new TemperatureFragment(), "fragment_temperature");
    }


    private void setInitialFragment(Fragment fragment, int menuPosition){
        mBottomNavigation.setSelectedIndex(menuPosition, true); // Setting temperature fragment first
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.contentFragment, fragment);
        transaction.commit();
    }


    public void switchFragment(Fragment fragmnet, String tag) {
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.contentFragment, fragmnet, tag).commit();
    }

    @Override
    public void onTemperatureFragmentInteraction(Uri uri) {

    }

    @Override
    public void onDevicesFragmentInteraction(Uri uri) {

    }

    @Override
    public void onAboutFragmentInteraction(Uri uri) {

    }
}
