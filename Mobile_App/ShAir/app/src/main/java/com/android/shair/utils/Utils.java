package com.android.shair.utils;

import android.content.Context;
import android.provider.Settings;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.Locale;

public class Utils {
    public final static String DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    public static String getAndroidId(Context context){
        return Settings.Secure.getString( context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static Date getDateFromString(String dateStr){
        if(dateStr == null){
            return  new Date(Long.MIN_VALUE);
        }
        SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);
        try {
            return format.parse(dateStr);
        } catch (Exception e) {
            return null;
        }
    }

    public static String getStringFromDate(Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        return dateFormat.format(date);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
