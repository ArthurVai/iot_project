package com.android.shair.service;

import android.util.Log;

import com.android.shair.models.Device;
import com.android.shair.models.DeviceTemperature;
import com.android.shair.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class ResponseParser {
    private static ResponseParser single_instance = null;

    private ResponseParser() {
    }

    public static ResponseParser getInstance() {
        if (single_instance == null) {
            single_instance = new ResponseParser();
        }
        return single_instance;
    }

    // Enrich device info
    public void parseJsonDevice(JSONObject jsonDevice, Device out) throws JSONException {
        try {
            out.setBtMac(((JSONObject) jsonDevice.get("location")).get("btMac").toString());
            out.setRequestedTemp(jsonDevice.getDouble("userTmpRequest"));
            Double realTemp = ((JSONObject) jsonDevice.get("tmp")).getDouble("realTmp");
            String realTempTimeStr =((JSONObject) jsonDevice.get("tmp")).getString("realTmpMeasureTime");
            Date realTempTime = Utils.getDateFromString(realTempTimeStr);
            DeviceTemperature temp = new DeviceTemperature(realTemp, realTempTime);
            out.setTemperature(temp);

            out.setVotedLikeUser(jsonDevice.getDouble("votedLikeUser"));
            String location = parseJsonLocation((JSONObject) jsonDevice.get("location"));
            out.getBTDevice().setLocation(location); // ensure location
        } catch (JSONException e) {
            Log.e("APIHandler",String.format("Couldn't parse device info, %s", e.getMessage()));
            throw e;
        }
    }

    public String parseJsonLocation(JSONObject jsonLocation) throws JSONException {
        String organization = jsonLocation.getString("organization");
        String building = jsonLocation.getString("building");
        int floor = jsonLocation.getInt("floor");
        String room = jsonLocation.getString("room");
        return String.format("Located at %s organization, %s building, %d's floor, %s room", organization, building, floor, room);
    }

}
