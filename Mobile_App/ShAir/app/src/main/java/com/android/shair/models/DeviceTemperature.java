package com.android.shair.models;

import java.util.Date;

public class DeviceTemperature {
    private double realTemp;
    private Date realTempMeasureTime;

    public DeviceTemperature(double realTemp, Date realTempMeasureTime) {
        this.realTemp = realTemp;
        this.realTempMeasureTime = realTempMeasureTime;
    }
    public double getRealTemp() {
        return realTemp;
    }

    public void setRealTemp(double realTemp) {
        this.realTemp = realTemp;
    }

    public Date getRealTempMeasureTime() {
        return realTempMeasureTime;
    }

    public void setRealTempMeasureTime(Date realTempMeasureTime) {
        this.realTempMeasureTime = realTempMeasureTime;
    }
}
