package com.android.shair.service.responses;

public class BaseResponse {
    private boolean mSuccess;
    private String mContent;

    public BaseResponse(boolean success, String content) {
        this.mSuccess = success;
        this.mContent = content;
    }

    public boolean isSuccess() {
        return mSuccess;
    }

    public String getContent() {
        return mContent;
    }
}
