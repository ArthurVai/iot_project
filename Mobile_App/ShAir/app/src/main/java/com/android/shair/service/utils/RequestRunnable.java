package com.android.shair.service.utils;

import android.os.Handler;
import android.os.Message;

import java.util.concurrent.Callable;

public class RequestRunnable implements Runnable{
    private Handler mHandler;
    private Callable<Object> mCallable;

    public RequestRunnable(Handler handler, Callable<Object> collable) {
        this.mHandler = handler;
        this.mCallable = collable;
    }

    @Override
    public void run() {
        boolean success = true;
        Object message = null;
        Message completeMessage = null;
        try {
            message = mCallable.call();
        } catch (Exception e) {
            success = false;
            message = e.getMessage();
        } finally {
            int state = success ? 1 : 0;
            completeMessage = this.mHandler.obtainMessage(state, message);
            completeMessage.sendToTarget();
        }
    }
}