package com.android.shair.models;

// Arduino device
public class Device {
    private String mBtMac;
    private double mRequestedTemp;
    private double mVotedLikeUser;
    private BTDevice mBtDevice;
    private DeviceTemperature temperature;

    public Device(BTDevice baseDeviceInfo) {
        this.mBtDevice = baseDeviceInfo;
    }

    public Device(String mBtMac, double requestedTemp, double votedLikeUser, BTDevice baseDeviceInfo, DeviceTemperature temperature) {
        this.mBtMac = mBtMac;
        this.mRequestedTemp = requestedTemp;
        this.mVotedLikeUser = votedLikeUser;
        this.mBtDevice = baseDeviceInfo;
        this.temperature = temperature;
    }

    public String getBtMac() {
        return mBtMac;
    }

    public void setBtMac(String mBtMac) {
        this.mBtMac = mBtMac;
    }

    public double getRequestedTemp() {
        return mRequestedTemp;
    }

    public void setRequestedTemp(double mRequestedTemp) {
        this.mRequestedTemp = mRequestedTemp;
    }

    public double getVotedLikeUser() {
        return mVotedLikeUser;
    }

    public void setVotedLikeUser(double mVotedLikeUser) {
        this.mVotedLikeUser = mVotedLikeUser;
    }

    public DeviceTemperature getTemperature() {
        return temperature;
    }

    public void setTemperature(DeviceTemperature temperature) {
        this.temperature = temperature;
    }

    public BTDevice getBTDevice() {
        return mBtDevice;
    }

    public void setBTDevice(BTDevice mBaseDeviceInfo) {
        this.mBtDevice = mBaseDeviceInfo;
    }
}
