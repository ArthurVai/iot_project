package com.android.shair.ui.fragments;

import android.content.Context;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.shair.R;
import com.android.shair.handlers.DeviceDisconnectHandler;
import com.android.shair.models.BTDevice;
import com.android.shair.service.responses.InfoResponse;
import com.android.shair.data.PreferencesManager;
import com.android.shair.models.Device;
import com.android.shair.service.APIHandler;
import com.android.shair.ui.dialogs.LoaderDialog;
import com.android.shair.utils.Utils;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import com.ramotion.fluidslider.FluidSlider;

import java.text.SimpleDateFormat;

import it.sephiroth.android.library.tooltip.Tooltip;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TemperatureFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TemperatureFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TemperatureFragment extends Fragment {
    public final static int MINIMUM_SLIDER_TEMPERATURE = 16;
    public final static int MAXIMUM_SLIDER_TEMPERATURE = 30;
    public final static int INITIAL_SLIDER_TMP = 25;
    private final static String DATE_PATTERN = "EEE, d MMM yyyy HH:mm:ss";
    public final static int REFRESH_TIME = 1000 * 60 * 1;
    private TextView mTextMessage;
    private OnFragmentInteractionListener mListener;
    private Device deviceInfo;
    public DeviceDisconnectHandler deviceDisconnectHandler;
    public static Handler refreshHandler;
    public static Handler sendTemperatureHandler;
    private FluidSlider slider = null;
    private LoaderDialog mLoaderDialog;
    private Tooltip.TooltipView mLocationTooltip;
    private Tooltip.TooltipView mRelevantTimeTooltip;
    private Context mContext;
    private Runnable mAutoRefresh = null;

    public TemperatureFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TemperatureFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TemperatureFragment newInstance(String param1, String param2) {
        TemperatureFragment fragment = new TemperatureFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        deviceInfo = PreferencesManager.getDevice(mContext);
        if (deviceInfo != null) {
            setHasOptionsMenu(true);
        }
        return inflater.inflate(R.layout.fragment_temperature, container, false);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        mTextMessage = (TextView) getView().findViewById(R.id.message);
        View content = getView().findViewById(R.id.content);
        View emptyContent = getView().findViewById(R.id.empty_content);

        registerHandlers();

        if (deviceInfo == null) {
            content.setVisibility(View.INVISIBLE);
            emptyContent.setVisibility(View.VISIBLE);
        } else {
            content.setVisibility(View.VISIBLE);
            emptyContent.setVisibility(View.INVISIBLE);
            initViews();
        }
        mLoaderDialog = new LoaderDialog(mContext);
        autoRefresh();
    }

    // Register handlers for handle responses from other threads
    private void registerHandlers() {
        // on disconnecting
        deviceDisconnectHandler = new DeviceDisconnectHandler(Looper.getMainLooper(), mContext) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                mLoaderDialog.hide();
                if (msg.what == 1) { // success
                    reload();
                }

            }
        };

        sendTemperatureHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                mLoaderDialog.hide();
                InfoResponse response = (InfoResponse) msg.obj;
                if (msg.what == 1 && response.isSuccess()) { // success
                    PreferencesManager.updateDevice(mContext, response.getDeviceInfo());
                } else {
                    if (!response.isDeviceConnected()) {
                        PreferencesManager.updateBTDevice(mContext, null);
                    }
                }
                reload();
            }
        };

        refreshHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                mLoaderDialog.hide();
                InfoResponse response = (InfoResponse) msg.obj;
                if (msg.what == 1 && response.isSuccess()) { // success
                    PreferencesManager.updateDevice(mContext, response.getDeviceInfo());
                } else {
                    if (!response.isDeviceConnected()) {
                        PreferencesManager.updateBTDevice(mContext, null);
                    }
                }
                reload();
            }
        };
    }

    // On clicking top appbar menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_disconnect:
                mLoaderDialog.setMessage(getResources().getString(R.string.disconnect_loader_message));
                mLoaderDialog.show();
                APIHandler.getInstance().disconnect(mContext, deviceDisconnectHandler, null);
                return true;
            case R.id.action_refresh:
                refresh();
                return true;
        }

        return super.onOptionsItemSelected(item); // important line
    }

    private void refresh() {
        mLoaderDialog.setMessage(getResources().getString(R.string.refresh_loader_message));
        mLoaderDialog.show();
        BTDevice info = PreferencesManager.getBtDevice(mContext);
        APIHandler.getInstance().refresh(mContext, TemperatureFragment.refreshHandler, info);
    }

    // Init UI controls
    private void initViews() {

        // init circular progress bar
        CircularProgressBar circularProgressBar = (CircularProgressBar) getView().findViewById(R.id.circularProgressbar);
        circularProgressBar.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        circularProgressBar.setBackgroundColor(ContextCompat.getColor(mContext, R.color.fbutton_color_emerald_light));
        circularProgressBar.setProgressBarWidth(getResources().getDimension(R.dimen.progressBarWidth));
        circularProgressBar.setBackgroundProgressBarWidth(getResources().getDimension(R.dimen.backgroundProgressBarWidth));
        int animationDuration = 2500;
        int percentage = (int) (deviceInfo.getVotedLikeUser() * 100);
        int temperature = (int) deviceInfo.getRequestedTemp();
        circularProgressBar.setProgressWithAnimation(percentage, animationDuration);

        // update text views
        TextView mTextTemperature = (TextView) getView().findViewById(R.id.temperatureTxt);
        TextView mTextPercentage = (TextView) getView().findViewById(R.id.percentageTxt);
        TextView mTextCurrent = (TextView) getView().findViewById(R.id.txtVotedTemperature);
        TextView txtConnectedTo = (TextView) getView().findViewById(R.id.txtConnectedTo);

        String realTemp = Utils.round(deviceInfo.getTemperature().getRealTemp(), 1) + "\u2103";
        mTextTemperature.setText(realTemp);
        mTextPercentage.setText(percentage + "%  Vote like you");
        String votedMessage = (int) temperature == 0 ? "Not voted yet" : "You voted " + (int) temperature + "\u2103";
        mTextCurrent.setText(votedMessage);
        final BTDevice baseInfo = deviceInfo.getBTDevice();
        txtConnectedTo.setText(String.format("Connected to %s", baseInfo.getBtName()));

        // init tooltips
        txtConnectedTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (null != mLocationTooltip) {
                    mLocationTooltip.hide();
                    mLocationTooltip = null;
                } else {

                    showTooltip(mLocationTooltip, getView().findViewById(R.id.txtConnectedTo), baseInfo.getLocation());
                }
            }
        });

        mTextTemperature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (null != mRelevantTimeTooltip) {
                    mRelevantTimeTooltip.hide();
                    mRelevantTimeTooltip = null;
                } else {
                    String relevantTime = new SimpleDateFormat(DATE_PATTERN).format(deviceInfo.getTemperature().getRealTempMeasureTime());
                    showTooltip(mRelevantTimeTooltip, getView().findViewById(R.id.temperatureTxt), "relevant to " + relevantTime);
                }
            }
        });

        // init send button
        Button btnSend = (Button) getView().findViewById(R.id.set_temperature_btn);
        btnSend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mLoaderDialog.setMessage(getResources().getString(R.string.send_loader_message));
                mLoaderDialog.show();
                BTDevice info = PreferencesManager.getBtDevice(mContext);
                double requestedTemp = Double.parseDouble(slider.getBubbleText());
                APIHandler.getInstance().changeTemp(v.getContext(), info, requestedTemp);
            }
        });

        int sliderTemp = (int) temperature == 0 ? INITIAL_SLIDER_TMP : (int) temperature;
        initSlider(sliderTemp);
    }

    private void showTooltip(Tooltip.TooltipView tooltip, View view, String text) {
        if (null != tooltip && tooltip.isShown()) {
            return;
        }

        tooltip = Tooltip.make(mContext,
                new Tooltip.Builder(101)
                        .anchor(view, Tooltip.Gravity.TOP)
                        .closePolicy(Tooltip.ClosePolicy.TOUCH_INSIDE_NO_CONSUME, 0)
                        .activateDelay(800)
                        .showDelay(300)
                        .text(text)
                        .maxWidth(500)
                        .withArrow(true)
                        .withOverlay(true)
                        .withStyleId(R.style.ToolTipLayoutCustomStyle)
                        .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                        .build());
        tooltip.show();
    }

    private void reload() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this).attach(this).commit();
    }

    private void initSlider(int temp) {
        final int max = MAXIMUM_SLIDER_TEMPERATURE;
        final int min = MINIMUM_SLIDER_TEMPERATURE;
        final int total = max - min;

        slider = getView().findViewById(R.id.fluidSlider);
        float pos = (float)(temp - min)/total;
        slider.setPosition(pos);
        slider.setBubbleText(Integer.toString(temp));
        slider.setStartText(String.valueOf(min));
        slider.setEndText(String.valueOf(max));

        slider.setBeginTrackingListener(new Function0<Unit>() {
            @Override
            public Unit invoke() {
                return Unit.INSTANCE;
            }
        });

        slider.setEndTrackingListener(new Function0<Unit>() {
            @Override
            public Unit invoke() {
                return Unit.INSTANCE;
            }
        });

        slider.setPositionListener(new Function1<Float, Unit>() {
            @Override
            public Unit invoke(Float pos) {
                final String value = String.valueOf((int) (min + total * pos));
                slider.setBubbleText(value);
                return Unit.INSTANCE;
            }
        });
    }

    private void autoRefresh() {
        if (mAutoRefresh == null) {
            final Handler refreshHandler = new Handler();
            mAutoRefresh = new Runnable() {
                @Override
                public void run() {
                    try {
                        if (mContext != null && isFragmentActive()) { //
                            BTDevice info = PreferencesManager.getBtDevice(mContext);
                            if (info != null) {
                                refresh();
                            }
                        }

                    } catch (Exception e) {

                    }
                    refreshHandler.postDelayed(this, REFRESH_TIME);
                }
            };
            refreshHandler.postDelayed(mAutoRefresh, REFRESH_TIME);
        }
    }

    private boolean isFragmentActive() {
        Fragment frag = getFragmentManager().findFragmentByTag("fragment_temperature");
        return frag != null && frag.isResumed();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onTemperatureFragmentInteraction(Uri uri);
    }
}
