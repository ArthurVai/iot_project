package com.android.shair.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.shair.models.BTDevice;
import com.android.shair.models.Device;
import com.android.shair.models.DeviceTemperature;
import com.android.shair.utils.Utils;

import java.util.Date;

import static android.content.Context.MODE_PRIVATE;

public class PreferencesManager {

    private static final String PREFS_NAME = "SHAIR_PREFERENCES";
    private SharedPreferences.Editor mEditor;

    // Update bluetooth device connection
    public static void updateBTDevice(Context context, BTDevice btDevice){
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        String location = null;
        String btMac = null;
        String btName = null;
        Boolean isConnected = false;
        if(btDevice == null){ // empty values of older connection
            editor.putString("requestedTemp", null);
            editor.putString("realTemp", null);
            editor.putString("realTempTime", null);
            editor.putString("votedLikeUser", null);
        }else{
            location = btDevice.getLocation();
            btMac = btDevice.getBtMac();
            btName = btDevice.getBtName();
            isConnected = true;
        }

        editor.putString("connected", Boolean.toString(isConnected));
        editor.putString("location", location);
        editor.putString("btMac", btMac);
        editor.putString("btName", btName);
        editor.apply();
    }

    // Get connected blutetooth device info
    public static BTDevice getBtDevice(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        boolean isConnected = Boolean.parseBoolean(prefs.getString("connected", "false"));
        if(!isConnected){
            return null;
        }
        String location =  prefs.getString("location", null);
        String btMac = prefs.getString("btMac", null);
        String btName = prefs.getString("btName", null);
        return new BTDevice(btMac, btName, location);
    }

    // Update details for connected arduino device
    public static void updateDevice(Context context , Device info) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        String requestedTemp = Double.toString(info.getRequestedTemp());
        String realTemp = Double.toString(info.getTemperature().getRealTemp());
        String realTempTime = Utils.getStringFromDate(info.getTemperature().getRealTempMeasureTime());
        String votedLikeUser = Double.toString(info.getVotedLikeUser());

        editor.putString("requestedTemp", requestedTemp);
        editor.putString("realTemp", realTemp);
        editor.putString("realTempTime", realTempTime);
        editor.putString("votedLikeUser", votedLikeUser);
        editor.apply();
    }

    // Get details for connected arduino device
    public static Device getDevice(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        boolean isConnected = Boolean.parseBoolean(prefs.getString("connected", "false"));
        if(!isConnected){
            return null;
        }

        String location =  prefs.getString("location", null);
        Double requestedTemp = Double.parseDouble(prefs.getString("requestedTemp", null));
        double realTemp =  Double.parseDouble(prefs.getString("realTemp", null));
        Date realTempTime = Utils.getDateFromString(prefs.getString("realTempTime", null));
        double votedLikeUser = Double.parseDouble(prefs.getString("votedLikeUser", null));
        String btMac = prefs.getString("btMac", null);
        String btName = prefs.getString("btName", null);
        BTDevice btDevice = new BTDevice(btMac, btName,location);
        DeviceTemperature temperature = new DeviceTemperature(realTemp, realTempTime);
        return new Device(btMac, requestedTemp, votedLikeUser, btDevice, temperature);
    }
}
