package com.android.shair.ui.dialogs;

import android.app.ProgressDialog;
import android.content.Context;

import com.android.shair.R;

public class LoaderDialog {
    private Context mContext;
    private ProgressDialog mDialog;
    private String defaultMessage;

    public LoaderDialog(Context context) {
        this.mContext = context;
        mDialog = new ProgressDialog(mContext);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        defaultMessage = mContext.getResources().getString(R.string.default_loader_message);
        mDialog.setMessage(defaultMessage);
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);
    }

    public void setMessage(String message) {
        if (message == null) {
            mDialog.setMessage(defaultMessage);
        } else {
            mDialog.setMessage(message);
        }
    }

    public void show() {
        this.mDialog.show();
    }

    public void hide() {
        this.mDialog.hide();
    }
}
