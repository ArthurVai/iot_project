package com.android.shair.handlers;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.android.shair.data.PreferencesManager;

public class DeviceDisconnectHandler extends Handler {
    private Context mContext;

    public DeviceDisconnectHandler(Looper looper, Context context) {
        super(looper);
        mContext = context;
    }

    @Override
    public void handleMessage(Message msg) {
       if(msg.what == 1){ // Success
           PreferencesManager.updateBTDevice(mContext, null);
       }
    }
}
