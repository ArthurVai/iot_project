package com.android.shair.ui.fragments.Devices;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.android.shair.R;
import com.android.shair.data.PreferencesManager;
import com.android.shair.handlers.DeviceDisconnectHandler;
import com.android.shair.models.BTDevice;
import com.android.shair.service.APIHandler;
import com.android.shair.ui.activities.MainActivity;
import com.android.shair.utils.Utils;
import com.kyleduo.switchbutton.SwitchButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DevicesRecyclerViewAdapter extends RecyclerView.Adapter<DevicesRecyclerViewAdapter.ViewHolder> {

    private Context mContext;
    private List<BTDevice> mDataList;
    private DevicesFragment mFragment;

    public DevicesRecyclerViewAdapter(Context mContext, HashMap<String, BTDevice> mapData, DevicesFragment fragment) {
        this.mContext = mContext;
        this.mDataList = new ArrayList<>(mapData.values());
        this.mFragment = fragment;
    }

    public void setDataList(List<BTDevice> items) {
        mDataList.clear();
        mDataList.addAll(items);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.device_list_item, parent, false);
        return new DevicesRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final BTDevice deviceInfo = (BTDevice) mDataList.get(position);
        holder.mBtName.setText(deviceInfo.getBtName());
        holder.mBtMac.setText(deviceInfo.getBtMac());
        holder.mBtLocation.setText(deviceInfo.getLocation());


        BTDevice info = PreferencesManager.getBtDevice(mContext);
        if (info != null && info.getBtMac().equals(deviceInfo.getBtMac())) {
            holder.mSbConnect.setChecked(true);
        } else {
            holder.mSbConnect.setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mBtName;
        private TextView mBtMac;
        private TextView mBtLocation;
        private SwitchButton mSbConnect;
        public DeviceDisconnectHandler deviceDisconnectHandler;

        public ViewHolder(final View itemView) {
            super(itemView);
            mBtName = (TextView) itemView.findViewById(R.id.name);
            mBtMac = (TextView) itemView.findViewById(R.id.mac);
            mBtLocation = (TextView) itemView.findViewById(R.id.txt_location);
            mSbConnect = (SwitchButton) itemView.findViewById(R.id.sb_connect_device);
            mSbConnect.setLinkTextColor(mContext.getResources().getColor(R.color.sky_blue));
            if (mSbConnect != null) {
                mSbConnect.setOnClickListener(new CompoundButton.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onToggleConnect(view);
                    }
                });
            }
        }

        // Handle connection or disconnection from bluetooth device, disconnect older device
        private void onToggleConnect(View view) {
            SwitchButton sb = (SwitchButton) view;
            boolean isChecked = sb.isChecked();
            mFragment.loaderDialog.show();
            DeviceDisconnectHandler handler = mFragment.deviceDisconnectHandler;

            if (isChecked) { // connect device
                MainActivity.deviceId = Utils.getAndroidId(mContext);
                ViewGroup containerView = ((ViewGroup) view.getParent().getParent().getParent());
                String btMac = ((TextView) containerView.findViewById(R.id.mac)).getText().toString();
                String btName = ((TextView) containerView.findViewById(R.id.name)).getText().toString();
                String btLocation = ((TextView) containerView.findViewById(R.id.txt_location)).getText().toString();
                BTDevice info = new BTDevice(btMac, btName, btLocation);

                BTDevice connectedDevice = PreferencesManager.getBtDevice(view.getContext());
                if (connectedDevice != null) { // first disconnect existing connected device
                    APIHandler.getInstance().disconnect(view.getContext(), handler, info);
                } else {
                    APIHandler.getInstance().connect(view.getContext(), info);
                }

            } else { // disconnect Device
                APIHandler.getInstance().disconnect(view.getContext(), handler, null);
            }

        }


    }
}
