#include <Arduino.h>
#include <ESP8266WiFi.h>

#include "http_handler.h"
#include "IR_handler.h"

const char* ssid     = "WIFI_NAME";
const char* password = "WIFI_PASS";



#define SERVER_HOST "shairclimatecontrol.azurewebsites.net"
#define SERVER_URI "/device/sync"

#define SERVER_NEWLINE "\r\n"
#define SERVER_NEWLINE_LEN 2
#define BUFFER_SIZE 4096
#define MAX_SKIPPED_TRANSMIT_CYCLES 20 //How many times can the device skip retransmitting the same temperature code?

int last_transmitted_temp = -1;
int skipped_cycles = 0;

String device_key = "devicetest";

const char *host = SERVER_HOST;

StaticJsonBuffer<BUFFER_SIZE> jsonBuffer;
uint16_t code_buffer[BUFFER_SIZE];

//return the index in the response where the HTTP response body begins
int get_response_body_index(String response){
  int prev, cur =- 1;
  prev = response.indexOf(SERVER_NEWLINE);
  while(prev != -1){
    cur = response.indexOf(SERVER_NEWLINE, prev+SERVER_NEWLINE_LEN);
    if (cur == prev + SERVER_NEWLINE_LEN){
      //Found the body: 2 newlines's in a row
      return cur+SERVER_NEWLINE_LEN;
    }
    prev = cur;
  }
  Serial.println("Could not find body of response!");
  return -1;
}

void http_setup(){
  delay(100);
  ///////////////////////////////////////////////////////////////////
  // Connect to wifi network:

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Netmask: ");
  Serial.println(WiFi.subnetMask());
  Serial.print("Gateway: ");
  Serial.println(WiFi.gatewayIP());
  ///////////////////////////////////////////////////////////////////
}

//process and handle the response body of the server
int process_server_json(String body){
  jsonBuffer.clear();
  Serial.printf("\nParsing body of length %d:\n", body.length());
  Serial.println(body);
  const char *json = body.c_str();

  JsonObject& root = jsonBuffer.parseObject(json);

  Serial.print("root[codes]:");
  Serial.print(root["codes"].as<String>());
  Serial.println("");

  float usr_avg_tmp = root["tmp"]["usersAvgTmp"];
  if ((int)usr_avg_tmp == last_transmitted_temp && skipped_cycles < MAX_SKIPPED_TRANSMIT_CYCLES){
    //no need to retransmit same temperature
    skipped_cycles++;
    Serial.printf("Skipping transmit for the %d time\n", skipped_cycles);
    return 0;
  }
  //Otherwise - prepare to transmit!

  JsonArray& codes_array = root["codes"].as<JsonArray>();

  Serial.printf("Number of codes to transmit: %d\n", codes_array.size());

  for (int i=0; i<codes_array.size();i++){
    JsonArray& cur_code = codes_array[i].as<JsonArray>();
    Serial.printf("printing cur code of size: %d\n", cur_code.size());    
    for(int j=0;j<cur_code.size();j++){
      // init the code buffer
      code_buffer[j] = (uint16_t)cur_code.get<unsigned int>(j);
      Serial.printf("%d ", cur_code.get<unsigned int>(j));
    }
    //transmit this code:
    transmit_code(code_buffer, cur_code.size());
  }
  //We have just transmitted, reset globals.
  last_transmitted_temp = (int)usr_avg_tmp;
  skipped_cycles = 0;

  return 0;
}

int execute_server_command(float cur_tmp){
  Serial.print("connecting to ");
  Serial.println(host);
  
  // Use WiFiClient class to create TCP connections
  WiFiClientSecure client;
  const int connectionPort = 443;//https port
  int status = client.connect(host, connectionPort);
  if (!status) {
    Serial.printf("connection failed with status %d\n", status);
    return 1;
  }
  
  String data = "{\"deviceID\":\"" + device_key + "\", \"tmp\":{\"realTmp\":\""+String(cur_tmp)+"\"}}";
  // We now create a URI for the request
  String url = SERVER_URI;
  Serial.print("Requesting URL: ");
  Serial.println(url);
  String packet = String("POST ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Content-Length: " + String(data.length()) + "\r\n" +
               "Content-Type: application/json\n" +
               "Connection: close\r\n\r\n" +
                data;
  Serial.printf("Sending packet:\n%s\n", packet.c_str());
  // This will send the request to the server
  client.print(packet);
  delay(500);
  
  String page = client.readString();
  // Serial.printf("Parsing page of length %d:\n", page.length());
  // Serial.println(page);
  int body_idx = get_response_body_index(page);
  String body = page.substring(body_idx);
  
  process_server_json(body);
  
  delay(200);
  Serial.println("closing connection");
  return 0;
}