#include <IRremoteESP8266.h>
#include <IRsend.h>
#include <Arduino.h>


#include "IR_handler.h"
//Globals
IRsend irsend(IR_LED);  // Set the GPIO to be used to sending the message.

void setup_ir(){
  pinMode(IR_LED, OUTPUT);
  irsend.begin();
}


void transmit_code(uint16_t code[], unsigned int len){
  Serial.printf("Will transmit %d length signal in 1 sec...\n", len);
  delay(1000);
  irsend.sendRaw(code, len, 38);  // Send the code at 38 KHz
  delay(1000);
  // Serial.printf("Sent:");
  // for(int j=0;j<trans_len;j++){
  //   Serial.printf("%d ", trans_data[j]);
  // }
  // Serial.println("");
  // Serial.printf("Sent setting temp to %d\n", temp);
}