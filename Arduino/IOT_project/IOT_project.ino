#include <Arduino.h>
#include <ArduinoJson.h>

#include "TemperatureHandler.h"
#include "IR_handler.h"
#include "http_handler.h"




float cur_temp_global = -1; // this holds the last temp we measured.


void setup() {
  Serial.begin(115200);
  temp_setup();
  http_setup();
  setup_ir();
}


void loop() {
  cur_temp_global = getTemp();
  Serial.printf("Cur temp: %.2f\n", cur_temp_global);
  execute_server_command(cur_temp_global);
  delay(100);
}
