﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace ShAirManager.API
{
    public class APIHandler
    {
        private static readonly APIHandler instance = new APIHandler();

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static APIHandler()
        {
        }

        private APIHandler()
        {
        }

        public static APIHandler Instance
        {
            get
            {
                return instance;
            }
        }

        public Response GetSupportedAcs()
        {
            bool success = false;
            string content = null;
            try
            {
                using (WebClient wc = new WebClient())
                {
                    string url = ConfigurationManager.AppSettings["ShAirApiGetSupportedAcsUrl"];
                    content = wc.DownloadString(url);                    
                    success = true;
                }
            }
            catch (Exception e)
            {
                content = e.Message;                
            }
            return new Response(){ Success = success, Content = content};
        }

        public Response Configure(string json)
        {
            bool success = false;
            string content = null;
            try
            {
                using (WebClient wc = new WebClient())
                {
                    string url = ConfigurationManager.AppSettings["ShAirApiConfigure"];                   
                    var http = (HttpWebRequest)WebRequest.Create(new Uri(url));
                    http.Accept = "application/json";
                    http.ContentType = "application/json";
                    http.Method = "POST";                    
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    Byte[] bytes = encoding.GetBytes(json);

                    Stream newStream = http.GetRequestStream();
                    newStream.Write(bytes, 0, bytes.Length);
                    newStream.Close();

                    var response = http.GetResponse();

                    var stream = response.GetResponseStream();
                    var sr = new StreamReader(stream);
                    content = sr.ReadToEnd();                    
                    success = true;
                }
            }
            catch (WebException e)
            {
                HttpWebResponse res = (HttpWebResponse)e.Response;
                if (((int)res.StatusCode).Equals(400))
                {
                    content = "Forbidden, please insert valid supported air conditioners";
                }
                else if (((int)res.StatusCode).Equals(403))
                {
                    content = "Forbidden, please insert a valid device secret key";
                }
                else {
                    content = e.Message;
                }

            }
            catch (Exception e)
            {
                content = e.Message;                
            }
            return new Response() { Success = success, Content = content };
        }
    }
}