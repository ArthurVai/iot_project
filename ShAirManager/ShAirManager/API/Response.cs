﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShAirManager
{
    public class Response
    {
        public bool Success{ get; set; }

        public string Content { get; set; }
    }
}