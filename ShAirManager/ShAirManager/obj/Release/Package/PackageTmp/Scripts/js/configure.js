﻿var acTags;
$(document).ready(function () {
    jQuery(document).ready(function () {
        // Tags Input
        acTags = jQuery('#tags').tagsInput({ width: 'auto', 'defaultText': 'ShAir IDs' });
    });

    getSupportedAcs();

    $("#btn-configure").click(function () {
        var secretKey = $.trim($("#secret-key").val());
        if ($.trim($("#secret-key").val()) == "") {
            alert("You must insert secret key");
            return;
        }

        var organization = $.trim($("#organization").val());
        if ($.trim($("#organization").val()) == "") {
            alert("You must insert organization");
            return;
        }

        var building = $.trim($("#building").val());
        if ($.trim($("#building").val()) == "") {
            alert("You must insert building");
            return;
        }

        var floor = $.trim($("#floor").val());
        if (floor == "" && !$.isNumeric(floor)) {
            alert("You must insert integer floor");
            return;
        }

        var room = $.trim($("#room").val());
        if (room == "") {
            alert("You must insert room");
            return;
        }

        var acArr = $(acTags).val().split(',');
        if (acArr.length == 1 && acArr[0] == "") {
            alert("You must define ac ids");
            return;
        }

        configure(secretKey, organization, building, floor, room, acArr);
    });

    $("#btn-delete").click(function () {
        var secretKey = $.trim($("#secret-key-delete").val());
        if ($.trim($("#secret-key-delete").val()) == "") {
            alert("You must insert secret key");
            return;
        }

        configure(secretKey, "", "", "", 0, []);
    });

    $("#btn-get").click(function () {
        var secretKey = $.trim($("#secret-key").val());
        if (secretKey == "") {
            alert("You must insert secret key");
            return;
        }

        getInfo(secretKey);
    });
});

function getSupportedAcs() {
    var url = "/configure/GetSupportedAcs"
    $.ajax({
        dataType: 'json',
        type: 'GET',
        url: url,
        success: function (response) {
            try {
                if (response.Success) {
                    var supportedAcs = JSON.parse(response.Content);
                    var container = $("#ac-container");
                    for (i = 0; i < supportedAcs.length; i++) {
                        var item = supportedAcs[i];
                        var rowNum = i + 1;
                        var row = $("<tr>").append(
                            $("<td>").html(rowNum),
                            $("<td>").html(item.company),
                            $("<td>").html(item.model),
                            $("<td>").html(item.acId));
                        container.append($(row));
                    }
                } else {
                    console.log("Failure, on GetSupportedAcs: " + response.Content);
                }

            }
            catch (err) {
                console.log("Error occured on GetSupportedAcs: " + err.message);
            }
        },

    });
}

function getInfo(secretKey) {
    var data = {
        json: JSON.stringify({ "deviceID": secretKey })
    }
    var url = "/configure/Configure"
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: url,
        data: data,
        success: function (response) {
            try {
                if (response.Success) {
                    var content = JSON.parse(response.Content);
                    setValues(content.location.building, content.location.floor,
                        content.location.organization, content.location.room, content.acIdLst);
                } else {
                    setValues("", 0, "", "", []);
                    console.log("Failure, on getInfo: " + response.Content);
                }

            }
            catch (err) {
                setValues("", 0, "", "", []);
                console.log("Error occured on getInfo: " + err.message);
            }
        },
        error: function (res) {
            setValues("", 0, "", "", []);
        }
    });
}

function setValues(building, floor, organization, room, acIdLst) {
    $("#building").val(building);
    $("#floor").val(floor);
    $("#organization").val(organization);
    $("#room").val(room);
    $(acTags).importTags('');
    if (acIdLst.length > 0) {
        for (i = 0; i < acIdLst.length; i++) {
            $(acTags).addTag(acIdLst[i]);
        }
    } 
}

function configure(secretKey, organization, building, floor, room, acs) {
    var acsJson = [];
    for (var i = 0; i < acs.length; i++) {
        acsJson.push({ "acId": acs[i] });
    }

    var data = {
        json: JSON.stringify({
            "deviceID": secretKey,
            "location": {
                "organization": organization, "building": building, "floor": floor, "room": room
            },
            "acs": acsJson
        })
    };

    var url = "/configure/Configure"
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: url,
        data: data,
        success: function (response) {
            try {
                if (response.Success) {
                    alert("Action completed successfuly!");
                    location.reload();
                } else {
                    console.log("Failure, on getInfo: " + response.Content);
                    alert("Action failed: " + response.Content);
                }
            }
            catch (err) {
                console.log("Error occured on getInfo: " + err.message);
            }
        },
    });

}

