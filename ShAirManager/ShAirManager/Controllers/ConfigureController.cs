﻿using ShAirManager.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShAirManager.Controllers
{
    public class ConfigureController : Controller
    {
        // GET: Configure
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetSupportedAcs()
        {
            return Json(APIHandler.Instance.GetSupportedAcs(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Configure(string json)
        {
            return Json(APIHandler.Instance.Configure(json), JsonRequestBehavior.AllowGet);
        }
    }
}