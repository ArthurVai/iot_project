﻿using System.Web;
using System.Web.Optimization;

namespace ShAirManager
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/js/jquery.min.js"));

  
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/js/modernizr.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(                      
                      
                      "~/Scripts/js/bootstrap.min.js",
                      "~/Scripts/js/waves.js",
                      "~/Scripts/js/wow.min.js",
                      "~/Scripts/js/jquery.nicescroll.js",
                      "~/Scripts/js/jquery.scrollTo.min.js",
                      "~/Scripts/plugins/jquery-detectmobile/detect.js",
                      "~/Scripts/plugins/fastclick/fastclick.js",
                      "~/Scripts/plugins/jquery-slimscroll/jquery.slimscroll.js",
                      "~/Scripts/plugins/jquery-blockui/jquery.blockUI.js",
                      "~/Scripts/plugins/tagsinput/jquery.tagsinput.min.js",                      
                      "~/Scripts/js/jquery.app.js",
                      "~/Scripts/plugins/jquery.validate/jquery.validate.min.js",
                      "~/Scripts/plugins/jquery.validate/form-validation-init.js"
                      ));

            bundles.Add(new StyleBundle("~/Styles/css").Include(
                      "~/Styles/bootstrap.min.css",
                      "~/Scripts/plugins/font-awesome/css/font-awesome.min.css",
                      "~/Scripts/plugins/ionicon/css/ionicons.min.css",
                      "~/Styles/material-design-iconic-font.min.css",
                      "~/Styles/animate.css",
                      "~/Styles/waves-effect.css",
                      "~/Styles/helper.css",
                      "~/Styles/style.css",
                      "~/Scripts/plugins/tagsinput/jquery.tagsinput.css"));

            bundles.Add(new ScriptBundle("~/Scripts/content").Include(
                     "~/Scripts/js/configure.js"));
        }
    }
}
