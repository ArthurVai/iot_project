package com;

import com.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController
public class UserController {

    @Autowired
    private SqlHandler sqlHandler;

    //TODO check user id
    @PostMapping("/connect")
    public ResponseEntity<User> connectNewUser(@RequestBody User u){
            if(u.getUserMac().isEmpty() || u.getLocation().getBtMac().isEmpty()){
                return ResponseEntity.badRequest().build();
            }
            else{
                try{
                    return ResponseEntity.ok(sqlHandler.createUser(u));
                }
                catch(com.SqlHandler.SqlHandlerException e)
                {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
                }

            }
    }


    @PostMapping("/changetmp")
    public ResponseEntity changeTemp(@RequestBody User u){
        //TODO CHECK IF I HAVE ALL ELEMENTS AND CONNECTED
        try{
            if (sqlHandler.checkUserConnection(u)){
                if(u.getUserTmpRequest()>=16 && u.getUserTmpRequest()<=30){
                        return ResponseEntity.ok(sqlHandler.updateUserTmp(u));
                }
                else{
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("user tmp out of range");
                }
            }
            else{
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("user is not connected");
            }
        }
        catch (SqlHandler.SqlHandlerException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/location")
    public ResponseEntity changeTemp(@RequestParam(value="btMac") String btMac){
        try{
            return ResponseEntity.ok(sqlHandler.getBtLocation(btMac));
        }
        catch(SqlHandler.SqlHandlerException e)
        {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @PostMapping("/refresh")
    public ResponseEntity refresh(@RequestBody User u){
        try{
            if(sqlHandler.checkUserConnection(u)){
                return ResponseEntity.ok(sqlHandler.refreshUser(u));
            }
            else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("user is not connected");
            }
        }
        catch(SqlHandler.SqlHandlerException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @PostMapping("/disconnect")
    public ResponseEntity disconnect(@RequestBody User u){
        //TODO CHECK USER INPUT
        try{
            if(sqlHandler.checkUserConnection(u)){
                sqlHandler.disconnectUser(u);
                return ResponseEntity.ok(HttpStatus.OK);
            }
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("user is not connected");

        }
        catch(SqlHandler.SqlHandlerException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
