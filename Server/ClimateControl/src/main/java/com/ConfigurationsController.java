package com;

import com.entities.AC;
import com.entities.Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/configure")
public class ConfigurationsController {

    @Autowired
    private SqlHandler sqlHandler;


    @GetMapping("/aclst")
    public ResponseEntity getAcList(){
        try{
            return ResponseEntity.ok(sqlHandler.getAcLst());
        }
        catch(SqlHandler.SqlHandlerException e)
        {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/device")
    public ResponseEntity configureNewDevice(@RequestBody Device d){
        try{
            if(sqlHandler.DeviceExistInDb(d)) {
                if(d.getAcs()!=null){//check legit input
                    List<Integer> deviceAcIdLst = d.getAcIdLst();
                    List<AC> allAcs = sqlHandler.getAcLst();
                    List<Integer> allAcsIds = new ArrayList<>();
                    for(AC ac : allAcs){
                        allAcsIds.add(ac.getAcId());
                    }

                    for(Integer id : deviceAcIdLst){
                        if(!allAcsIds.contains(id)){
                            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("acID "+id+" does not exist");

                        }
                    }
                }

                return ResponseEntity.ok(sqlHandler.configureDevice(d));
            }
            else{
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("device does not exist");
            }
        }
        catch(SqlHandler.SqlHandlerException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
