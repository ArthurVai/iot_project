package com.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Device {

    private String deviceID;
    private Temperature tmp;
    private Location location;
    private List<AC> acs;
    private List<List<Integer>> codes;


    public List<Integer> getAcIdLst(){
        List<Integer> aclst = new ArrayList<>();
        for (AC ac:acs){
            aclst.add(ac.getAcId());
        }
        return aclst;
    }

}
