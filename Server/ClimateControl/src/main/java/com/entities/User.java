package com.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private String userMac;
    private Location location;
    private Temperature tmp;
    private float userTmpRequest;
    private float votedLikeUser;

}
