package com.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Temperature {

    private float realTmp;
    private Timestamp realTmpMeasureTime;
    private float usersAvgTmp;
}
