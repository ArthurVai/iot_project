package com;

import com.entities.*;
import com.sun.org.apache.bcel.internal.generic.RETURN;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


//@Scope(value = "prototype")
@Component
public class SqlHandler {

    static final String SQL_SERVER = "climatecontrolsqlserver.database.windows.net:1433";
    static final String USER_DB = "ClimateControlSqlDB";
    static final String USER_NAME = "danielsehaik";
    static final String PASSWORD = "Aa123456";

    private Connection conn;
    private PreparedStatement ps;
    private ResultSet rs;

    public SqlHandler(){
        super();
        System.out.println("object created");
    }


    private void connectToServer() throws SqlHandlerException{
        try{
            this.conn = DriverManager.getConnection(
                    "jdbc:sqlserver://"+SQL_SERVER+";database="+USER_DB,
                    USER_NAME,
                    PASSWORD);
        }
        catch (SQLException e){
            throw new SqlHandlerException("can't connect to server");
        }
    }

    private void queryToServer(String query, List<String> parameters) throws SqlHandlerException{
        try{
            if(conn==null || conn.isClosed()){
                connectToServer();
            }
            String firstWord = query.split(" ", 2)[0];
            if(this.rs!=null){
                this.rs.close();
            }
            if(this.ps!=null){
                this.ps.close();
            }
            this.ps = conn.prepareStatement(query);
            if (parameters!=null){
                for(int i=1; i<=parameters.size(); i++){
                    ps.setString(i, parameters.get(i-1));
                }
            }
            if(firstWord.toLowerCase().equals("select")){
                this.rs = ps.executeQuery();
            }
            else{
                if(firstWord.toLowerCase().equals("update")){
                    ps.executeUpdate();
                }
                else{
                    ps.execute();
                }

            }
        }
        catch (SQLException e){
            try{
                if(ps!=null){
                    ps.close();
                    conn.close();
                }
                else{
                    if(conn!=null){
                        conn.close();
                    }
                }
            }catch (SQLException ep){
                throw new SqlHandlerException("can't close connections");
            }
            throw new SqlHandlerException("can't preform query");
        }
    }


    private void closeConnection() throws SQLException{
        try{
            if(this.rs!=null){
                this.rs.close();
            }
            if(this.ps!=null){
                this.ps.close();
            }
            if(this.conn!=null){
                this.conn.close();
            }
        }catch(SQLException e){
            throw e;
        }
    }



    public User createUser(User u) throws SqlHandlerException {
        try{

            u = fetchUserDetails(u);

            if(!checkUserConnection(u)){
                //insert user to user_tbl - if user is connected exception will be thrown as userMac is pk
                queryToServer("INSERT into user_tbl (userMac, btMac) values (?,?)",new ArrayList<>(Arrays.asList(u.getUserMac(),u.getLocation().getBtMac())));
            }

            closeConnection();

            return u;
        }
        catch(Exception e) {
            throw new SqlHandlerException("can't create new user");
        }
    }


    public User updateUserTmp(User u) throws SqlHandlerException {
        try{
            //update user tbl
            queryToServer("UPDATE user_tbl SET lastUserRequest=? WHERE userMac=?",new ArrayList<>(Arrays.asList(String.valueOf(u.getUserTmpRequest()),u.getUserMac())));

            u = fetchUserDetails(u);

            closeConnection();
            return u;

        } catch (Exception e) {
            throw new SqlHandlerException("can't update user");
        }
    }

    public User fetchUserDetails(User u) throws SqlHandlerException {
        try{

            //get user btmac and last request
            if(u.getLocation()==null || u.getLocation().getBtMac().isEmpty()){
                queryToServer("SELECT * FROM user_tbl WHERE userMac=?",new ArrayList<>(Arrays.asList(u.getUserMac())));
                rs.next();
                u.setUserTmpRequest(rs.getFloat("lastUserRequest"));
                u.setLocation(new Location());
                u.getLocation().setBtMac(rs.getString("btMac"));
            }

            //set user location and tempature
            u.setLocation(fetchLocationByBtMac(u.getLocation().getBtMac()));
            u.setTmp(fetchTmp(u.getLocation().getBtMac()));

            //calculate percentage that voted like user
            queryToServer("SELECT COUNT(userMac) FROM user_tbl WHERE btMac=?",new ArrayList<>(Arrays.asList(u.getLocation().getBtMac())));
            rs.next();
            float numberOfVotes = rs.getInt(1);
            queryToServer("SELECT COUNT(userMac) FROM user_tbl WHERE btMac=? AND lastUserRequest=?",new ArrayList<>(Arrays.asList(u.getLocation().getBtMac(),String.valueOf(u.getUserTmpRequest()))));
            rs.next();
            float numberVotedLikeUser = rs.getInt(1);
            u.setVotedLikeUser(numberVotedLikeUser/numberOfVotes);

            return u;
        }catch(Exception e){
            throw new SqlHandlerException("can't fetch user details");
        }

    }

    public Location getBtLocation(String btMac) throws SqlHandlerException{
        try{
            Location loc = fetchLocationByBtMac(btMac);
            closeConnection();
            return loc;
        }catch(Exception e){
            throw  new SqlHandlerException("can't get location");
        }
    }

    public boolean checkUserConnection(User u) throws SqlHandlerException{
        try{
            queryToServer("SELECT COUNT(userMac) FROM user_tbl WHERE userMac=?",new ArrayList<>(Arrays.asList(u.getUserMac())));
            rs.next();
            int numberFound = rs.getInt(1);
            closeConnection();
            if(numberFound==0){
                return false;
            }
            return true;
        }catch(Exception e){
            throw new SqlHandlerException("can't check user conection");
        }
    }

    public void removeOldUserConnections(Device d) throws SqlHandlerException{
        try{
            queryToServer("DELETE FROM user_tbl WHERE connectionTime < DATEADD(hh, -1, GETDATE())",null);
            closeConnection();
        }catch(Exception e){
            throw new SqlHandlerException("can't remove old devices");
        }
    }


    private Location fetchLocationByBtMac(String btmac) throws SqlHandlerException{
        try{
           // connectToServer();
            queryToServer("SELECT * FROM location_tbl WHERE btMac=?", new ArrayList<>(Arrays.asList(btmac)));
            rs.next();
            Location loc = new Location();
            loc.setBtMac(rs.getString("btMac").trim());
            loc.setOrganization(rs.getString("organization"));
            loc.setBuilding(rs.getString("building"));
            loc.setFloor(rs.getInt("floor"));
            loc.setRoom(rs.getString("room"));
            return loc;
        }catch (Exception e){
            throw new SqlHandlerException("can't fetch location");
        }
    }

    private Location fetchLocationByDevice(String deviceID) throws SqlHandlerException{
        try{
            queryToServer("SELECT * FROM location_tbl WHERE deviceID=?", new ArrayList<>(Arrays.asList(deviceID)));
            rs.next();
            Location loc = new Location();
            loc.setBtMac(rs.getString("btMac").trim());
            loc.setOrganization(rs.getString("organization"));
            loc.setBuilding(rs.getString("building"));
            loc.setFloor(rs.getInt("floor"));
            loc.setRoom(rs.getString("room"));
            return loc;
        }catch (Exception e){
            throw new SqlHandlerException("can't fetch location");
        }
    }


    public User refreshUser(User u) throws SqlHandlerException{
        try{
            u = fetchUserDetails(u);

            closeConnection();

            return u;

        }catch(Exception e){
            throw new SqlHandlerException("can't refresh user");
        }
    }

    public void disconnectUser(User u) throws SqlHandlerException{
        try{
            //update user tbl
            queryToServer("DELETE FROM user_tbl WHERE userMac=?",new ArrayList<>(Arrays.asList(u.getUserMac())));
            closeConnection();

        }catch(Exception e){
            throw new SqlHandlerException("can't disconnect user");
        }
    }

    private Temperature fetchTmp(String btmac) throws SqlHandlerException{
        try{
            Temperature tmp= new Temperature();
            queryToServer("SELECT TOP 1 * FROM temperature_tbl where deviceID IN (SELECT deviceID from location_tbl WHERE btMac=?) ORDER BY [measureTime] DESC", new ArrayList<>(Arrays.asList(btmac)));
            rs.next();
            tmp.setRealTmp(rs.getFloat("realTmp"));
            tmp.setRealTmpMeasureTime(rs.getTimestamp("measureTime"));
            tmp.setUsersAvgTmp(fetchUsersAvgTmpByBtMac(btmac));
            return tmp;
        }catch (Exception e){
            throw new SqlHandlerException("can't fetch temperature");
        }
    }


    private float fetchUsersAvgTmpByBtMac(String btmac) throws SqlHandlerException{
        try{
            queryToServer("SELECT AVG(CAST(lastUserRequest AS FLOAT)) FROM user_tbl WHERE btMac=? AND lastUserRequest IS NOT NULL",new ArrayList<>(Arrays.asList(btmac)));
            rs.next();
            return rs.getFloat(1);

        }catch (Exception e){
            throw new SqlHandlerException("can't fetch avg temperature");
        }
    }


    public Device syncDevice(Device d) throws SqlHandlerException{
        try{
            //inset temperature
            queryToServer("INSERT into temperature_tbl (deviceID, measureTime,realTmp) values (?,?,?)",
                    new ArrayList<>(Arrays.asList(d.getDeviceID(),String.valueOf(d.getTmp().getRealTmpMeasureTime()),String.valueOf(d.getTmp().getRealTmp()))));

            //get location details
            Location loc = fetchLocationByDevice(d.getDeviceID());

            //set user avg tmp
            d.getTmp().setUsersAvgTmp(fetchUsersAvgTmpByBtMac(loc.getBtMac()));


            queryToServer("SELECT * FROM device_ac_tbl WHERE deviceID=?",
                    new ArrayList<>(Arrays.asList(d.getDeviceID())));

            List<Integer> aclst = new ArrayList<>();
            while(rs.next()){
                aclst.add(rs.getInt("acID"));
            }

            List<List<Integer>> codes = new ArrayList<>();
            for(Integer ac: aclst){
                if(Math.round(d.getTmp().getUsersAvgTmp()) != 0 ){//check that avg is legit
                    queryToServer("SELECT * FROM ac_code_tbl WHERE acID=? AND tmp=?",
                            new ArrayList<>(Arrays.asList(String.valueOf(ac),String.valueOf(Math.round(d.getTmp().getUsersAvgTmp())))));
                    rs.next();
                    List<String> codeStrLst = Arrays.asList(rs.getString("code").split("\\s*,\\s*"));
                    List<Integer> codeLst = new ArrayList<>();
                    for(String code:codeStrLst){
                        codeLst.add(Integer.parseInt(code));
                    }
                    codes.add(codeLst);
                }
            }

            d.setLocation(new Location());
            d.setAcs(new ArrayList<>());
            d.setCodes(codes);
            closeConnection();
            return d;
        }
        catch (Exception e){
            throw new SqlHandlerException("can't sync device");
        }
    }

    public List<AC> getAcLst() throws SqlHandlerException{
        try{
            List<AC> acLst = new ArrayList<>();
            queryToServer("SELECT DISTINCT acID, company, model FROM ac_code_tbl",null);
            while (rs.next()){
                acLst.add(new AC(rs.getInt("acID"),rs.getString("company").trim(),rs.getString("model").trim()));
            }
            closeConnection();
            return acLst;
        }
        catch(Exception e){
            throw new SqlHandlerException("can't get ac list");
        }
    }


    public Device configureDevice(Device d) throws SqlHandlerException {
        try{
            if(d.getLocation()!=null){
                //update device location
                queryToServer("UPDATE location_tbl SET organization=?, building=?, floor=?, room=? WHERE deviceID=?",
                        new ArrayList<>(Arrays.asList(d.getLocation().getOrganization(),
                                d.getLocation().getBuilding(),
                                String.valueOf(d.getLocation().getFloor()),
                                d.getLocation().getRoom(),
                                d.getDeviceID())));
            }

            if(d.getAcs()!=null){
                //update device ac tbl
                queryToServer("DELETE FROM device_ac_tbl WHERE deviceID=?",new ArrayList<>(Arrays.asList(d.getDeviceID())));
                for(AC ac : d.getAcs()){
                    queryToServer("INSERT device_ac_tbl (deviceID, acID) values (?,?)",
                            new ArrayList<>(Arrays.asList(d.getDeviceID(), String.valueOf(ac.getAcId()))));
                }
            }

            d.setLocation(fetchLocationByDevice(d.getDeviceID()));


            //get all existing acs connected to device
            queryToServer("SELECT DISTINCT acID, company, model FROM ac_code_tbl WHERE acID IN (SELECT acID FROM device_ac_tbl WHERE deviceID=?)",
                    new ArrayList<>(Arrays.asList(d.getDeviceID())));
            List<AC> acLst = new ArrayList<>();
            while (rs.next()){
                acLst.add(new AC(rs.getInt("acID"),rs.getString("company").trim(),rs.getString("model").trim()));
            }
            d.setAcs(acLst);

            closeConnection();

            return d;
        }catch (Exception e){
            throw new SqlHandlerException("can't configue device");
        }
    }

    public boolean DeviceExistInDb(Device d) throws  SqlHandlerException{
        try{
            queryToServer("SELECT COUNT(deviceID) FROM location_tbl WHERE deviceID=?",new ArrayList<>(Arrays.asList(d.getDeviceID())));
            rs.next();
            int numberFound = rs.getInt(1);
            closeConnection();
            if(numberFound==0){
                return false;
            }
            return true;
        }catch (Exception e){
            throw new SqlHandlerException("can't check if device exist");
        }
    }


    public class SqlHandlerException extends Exception {
        public SqlHandlerException(String message) {
            super(message);
        }
    }


}
