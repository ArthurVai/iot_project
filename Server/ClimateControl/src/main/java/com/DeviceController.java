package com;

import com.entities.Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;


@RequestMapping("/device")
@RestController
public class DeviceController {

    @Autowired
    private SqlHandler sqlHandler;


    @PostMapping("sync")
    public ResponseEntity sync(@RequestBody Device device){
        if(device.getTmp().getRealTmp()==0 || device.getDeviceID().isEmpty())
        {
            return ResponseEntity.badRequest().body("missing arguments");
        }
        else{
            try{
                Timestamp sycTime = new Timestamp(System.currentTimeMillis());
                device.getTmp().setRealTmpMeasureTime(sycTime);
                sqlHandler.removeOldUserConnections(device);
                Device d = sqlHandler.syncDevice(device);
                return ResponseEntity.ok(d);
                }
                catch (SqlHandler.SqlHandlerException e){
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        }

    }


}

